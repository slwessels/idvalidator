########################################################################
#    To install before start up the service
########################################################################
# pip install falcon - framework
# pip install gunicorn - execute service
#########################
# Execute app as follow:
#########################
# gunicorn IDValidator:app


import falcon
import json

from IDValidationLogic import IDValidationLogic

# Curl request
#curl -H "Content-Type: application/json" -X POST -d '{"id":"8004015068087"}' http://localhost:8000/IDValidator

class IDValidatorResource(object):
    def on_get(self, req, resp):
        """Handles GET requests"""
        resp.status = falcon.HTTP_200
        resp.body = 'Hello world!'
 
    def on_post(self, req, resp):
        """Handles POST requests"""
        try:
            raw_json = req.stream.read()
        except Exception as ex:
            raise falcon.HTTPError(falcon.HTTP_400,
                'Error',
                ex.message)
        try:
            result_json = json.loads(raw_json, encoding='utf-8')
        except ValueError:
            raise falcon.HTTPError(falcon.HTTP_400,
                'Malformed JSON',
                'Could not decode the request body. The '
                'JSON was incorrect.')
 
        # Instantiate ValidatorLogic 
        validator = IDValidationLogic.IDValidatorLogic()

        # Do id verification
        returnValue = validator.extractData(result_json["id"])

        # Construct response message
        resp.status = falcon.HTTP_202
        resp.body = json.dumps(returnValue, encoding='utf-8')

# falcon.API instances are callable WSGI apps
app = falcon.API()

# Resources are represented by long-lived class instances
IDValidator = IDValidatorResource()
 
# things will handle all requests to the '/things' URL path
app.add_route('/IDValidator', IDValidator)
