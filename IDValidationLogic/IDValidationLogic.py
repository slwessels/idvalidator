import json

import datetime
# from datetime import date 
# from time import gmtime, strftime
import time
# from dateutil import parser

# https://mybroadband.co.za/news/government/176895-what-your-south-african-id-number-reveals-about-you.html

class IDValidatorLogic(object):
    def calculate_age(self,birth_date):
        today = datetime.datetime.today()
        age = today.year - birth_date.year
        # full_year_passed = (today.month, today.day) < (birth_date.month, birth_date.day)
        # if not full_year_passed:
        #     age -= 1
        return age

    def getGender(self,value):
        if (int(value) <= 4):
            return "Female"
        else:
            return "Male"

    def getCitizenship(self,value):
        if (int(value) == 0):
            return "South African"
        else:
            return "Foreigner"
    #8004015068087
    def validateID(self,IDNum): 
        """Validates a South African ID Number"""
        if (len(IDNum) != 13):
            return "Incorrect Length"
        if (int(IDNum[10]) > 1):
            return "Invalid Citizenship indicator "+ IDNum[10]

        # Do validation routine
        even = []
        odds = []

        for digit in range(0,12):
            if digit % 2 == 0: #Even digits
                even.append(int(IDNum[digit]))
            else:
                odds.append(int(IDNum[digit]))
        evenSum = sum(even)
        oddProduct = int(''.join(map(str, odds)))*2
        oddSum = sum(list(map(int,str(oddProduct))))

        productResult = evenSum+oddSum
        validationBit = 10 - int(str(productResult)[1])

        if(validationBit == int(IDNum[12])):
            return "Validated"
        else:
            return "Incorrect Validation bit"

    def extractData(self,id): 
        #8004015068087
        #8110220061088
        #1502260531086
        #1612060445088
        dateOfBirth = datetime.datetime.strptime(id[0:6],"%y%m%d").date()
        age = self.calculate_age(dateOfBirth)
        gender = self.getGender(id[6])
        citizenship = self.getCitizenship(id[10])
        validated = self.validateID(id)

        data = {}
        data['id'] = id
        data['dateOfBirth']= dateOfBirth.strftime("%Y-%m-%d")
        data['age'] = age
        data['gender'] = gender
        data['citizenship'] = citizenship
        data['validated'] = validated
        data['dob'] = id[0:6]
        data['genderbit'] = id[6]
        data['sequence'] = id[7:10]
        data['citizenbit'] = id[10]

        # data['even'] = even
        # data['odds'] = odds
        # data['evenSum'] = evenSum
        # data['oddSum'] = oddSum
        # data['productResult'] = productResult
        # data['oddProduct'] = oddProduct
        # data['validationBit'] = validationBit
        return data

